//
//  User.h
//  CoreDataHomeProject
//
//  Created by Vladimir on 02.05.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Phone;

@interface User : NSManagedObject

#pragma mark Properties

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * age;
@property (nonatomic, retain) NSSet *phones;
@end

@interface User (CoreDataGeneratedAccessors)

- (void)addPhonesObject:(Phone *)value;
- (void)removePhonesObject:(Phone *)value;
- (void)addPhones:(NSSet *)values;
- (void)removePhones:(NSSet *)values;

@end
