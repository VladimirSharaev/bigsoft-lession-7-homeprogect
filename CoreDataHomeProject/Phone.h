//
//  Phone.h
//  CoreDataHomeProject
//
//  Created by Vladimir on 02.05.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class User;

@interface Phone : NSManagedObject

#pragma mark Properties

@property (nonatomic, retain) NSString * number;
@property (nonatomic, retain) User *user;

@end
