//
//  Phone+Creation.h
//  CoreDataHomeProject
//
//  Created by Vladimir on 02.05.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import "Phone.h"

@interface Phone (Creation)

+ (Phone *)phoneWithNumber:(NSString *)number user:(User *)user;

@end
