//
//  AddPhoneViewController.h
//  CoreDataHomeProject
//
//  Created by Vladimir on 03.05.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AddPhoneViewControllerDelegate <NSObject>

- (void)addPhone:(NSString *)number;

@end

@interface AddPhoneViewController : UIViewController

@property (weak, nonatomic) id <AddPhoneViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UITextField *numberTextField;


- (IBAction)addNumberButtonWasPressed:(id)sender;

@end
