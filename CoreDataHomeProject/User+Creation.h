//
//  User+Creation.h
//  CoreDataHomeProject
//
//  Created by Vladimir on 02.05.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import "User.h"

@interface User (Creation)

+ (User *)userWithNameAndAge:(NSString *)name age:(NSNumber *)age;

@end
