//
//  AddUserViewController.m
//  CoreDataHomeProject
//
//  Created by Vladimir on 03.05.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import "AddUserViewController.h"

@implementation AddUserViewController

- (IBAction)addUserButtonWasPressed:(id)sender
{
    NSString *name = self.nameTextBox.text;
    NSNumber *age = [NSNumber numberWithInteger:[self.ageTextBox.text integerValue]];
    if ((![self.nameTextBox.text  isEqual: @""]) && (![self.ageTextBox.text  isEqual: @""])) {
        if (self.delegate) {
            [self.delegate addUser:name age:age];
        }
        self.nameTextBox.text = @"";
        self.ageTextBox.text = @"";
    }else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Enter correct date" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }

}
@end
