//
//  Phone+Creation.m
//  CoreDataHomeProject
//
//  Created by Vladimir on 02.05.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import "Phone+Creation.h"
#import "AppDelegate.h"

@implementation Phone (Creation)

+ (Phone *)phoneWithNumber:(NSString *)number user:(User *)user
{
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    NSManagedObjectContext *context = appDelegate.managedObjectContext;
    
    Phone *phone = (Phone *)[NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass(self) inManagedObjectContext:context];
    if (phone) {
        phone.number = number;
        phone.user = user;
    }
    return phone;
}

@end
