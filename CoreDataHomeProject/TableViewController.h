//
//  TableViewController.h
//  CoreDataHomeProject
//
//  Created by Vladimir on 02.05.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "AddUserViewController.h"
#import "PhoneTableViewController.h"


@interface TableViewController : UITableViewController <UITableViewDataSource, UITableViewDelegate,AddUserViewControllerDelegate>

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (nonatomic) NSArray *users;


- (IBAction)addUserButton:(id)sender;

- (void)reload;

@end
