//
//  User.m
//  CoreDataHomeProject
//
//  Created by Vladimir on 02.05.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import "User.h"
#import "Phone.h"


@implementation User

@dynamic name;
@dynamic age;
@dynamic phones;

@end
