//
//  PhoneCell.h
//  CoreDataHomeProject
//
//  Created by Vladimir on 03.05.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhoneCell : UITableViewCell

#pragma mark Properties

@property (weak, nonatomic) IBOutlet UILabel *numberLabel;

@end
