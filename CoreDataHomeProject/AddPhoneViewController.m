//
//  AddPhoneViewController.m
//  CoreDataHomeProject
//
//  Created by Vladimir on 03.05.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import "AddPhoneViewController.h"

@implementation AddPhoneViewController

- (IBAction)addNumberButtonWasPressed:(id)sender
{
    NSString *number = self.numberTextField.text;
    if (![self.numberTextField.text  isEqual: @""]) {
        if (self.delegate) {
            [self.delegate addPhone:number];
        }
        self.numberTextField.text = @"";
    }else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Enter correct date" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
}
@end
