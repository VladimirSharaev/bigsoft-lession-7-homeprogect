//
//  PhoneTableViewController.m
//  CoreDataHomeProject
//
//  Created by Vladimir on 03.05.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import "PhoneTableViewController.h"
#import "PhoneCell.h"
#import "AddPhoneViewController.h"
#include "AppDelegate.h"
#include "Phone+Creation.h"

@interface PhoneTableViewController () <AddPhoneViewControllerDelegate, UITableViewDataSource, UITableViewDelegate>

@property NSArray *phones;

@end

@implementation PhoneTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    self.managedObjectContext = appDelegate.managedObjectContext;
    
    [self reload];
}

- (void)reload
{
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([Phone class])];
    NSSortDescriptor *numberSort = [NSSortDescriptor sortDescriptorWithKey:@"number" ascending:YES];
    request.sortDescriptors = @[numberSort];
    [request setPredicate:[NSPredicate predicateWithFormat:@"user == %@", self.user]];
    NSArray *phones = [self.managedObjectContext executeFetchRequest:request error:nil];
    self.phones = phones;
  
    
    [self.tableView reloadData];
}

- (IBAction)addButtonWasPressed:(id)sender
{
    AddPhoneViewController *addPhoneCellViewController = [AddPhoneViewController new];
    UIStoryboard *storyboard = self.storyboard;
    addPhoneCellViewController = [storyboard instantiateViewControllerWithIdentifier:@"AddPhoneViewController"];
    addPhoneCellViewController.delegate = self;
    [self.navigationController pushViewController:addPhoneCellViewController animated:YES];
}

#pragma mark Table View Data Sorce

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.phones.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifierUserCell = @"PhoneCell";
    PhoneCell *cell = [tableView dequeueReusableCellWithIdentifier:identifierUserCell];
    if (!cell) {
        cell = [[PhoneCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifierUserCell];
    }
    cell.numberLabel.text = [self.phones[indexPath.row] number];
    return cell;
}


#pragma mark - Table View Delegate


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.managedObjectContext deleteObject:self.phones[indexPath.row]];
        [self reload];
        
        [self.managedObjectContext save:nil];
    }
}

#pragma mark Add Phone View Controller Delegate

- (void)addPhone:(NSString *)number
{
    [Phone phoneWithNumber:number user:self.user];
    [self reload];
    
    [self.managedObjectContext save:nil];
}
@end
