//
//  TableViewController.m
//  CoreDataHomeProject
//
//  Created by Vladimir on 02.05.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import "TableViewController.h"
#import "UserCell.h"
#import "User+Creation.h"
#import "AppDelegate.h"
#import "Phone.h"


@interface TableViewController ()


@end

@implementation TableViewController

static NSString *CellIndentifier = @"UserCellIndentifier";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.allowsMultipleSelectionDuringEditing = NO;
    
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    self.managedObjectContext = appDelegate.managedObjectContext;
    
    [self reload];
}



- (IBAction)addUserButton:(id)sender
{
    AddUserViewController *addUserViewController = [AddUserViewController new];
    UIStoryboard *storyboard = self.storyboard;
    addUserViewController = [storyboard instantiateViewControllerWithIdentifier:@"AddUserViewController"];
    addUserViewController.delegate = self;
    [self.navigationController pushViewController:addUserViewController animated:YES];
}

- (void)reload
{
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([User class])];
    NSSortDescriptor *nameSort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    request.sortDescriptors = @[nameSort];
    NSArray *users = [self.managedObjectContext executeFetchRequest:request error:nil];
    self.users = users;
    [self.tableView reloadData];
}

#pragma mark Add User View Delegate

- (void)addUser:(NSString *)name age:(NSNumber *)age
{
    [User userWithNameAndAge:name age:age];
    [self reload];
    
    [self.managedObjectContext save:nil];
}

#pragma mark Table View Data Sorce 

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.users.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{    
    NSString *identifierUserCell = @"UserCell";
    UserCell *cell = [tableView dequeueReusableCellWithIdentifier:identifierUserCell];
    if (!cell) {
        cell = [[UserCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifierUserCell];
    }
    cell.nameLabel.text = [self.users[indexPath.row] name];
    cell.ageLabel.text = [[self.users[indexPath.row] age] stringValue];
    return cell;
}

#pragma mark - Table View Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    PhoneTableViewController *phoneTebleViewController = [PhoneTableViewController new];
    UIStoryboard *storyboard = self.storyboard;
    phoneTebleViewController = [storyboard instantiateViewControllerWithIdentifier:@"PhoneTableViewController"];
    phoneTebleViewController.user = self.users[indexPath.row];
    [self.navigationController pushViewController:phoneTebleViewController animated:YES];
}

- (void)deselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([Phone class])];
        [request setPredicate:[NSPredicate predicateWithFormat:@"user == %@", self.users[indexPath.row]]];
        NSArray *phones = [self.managedObjectContext executeFetchRequest:request error:nil];
        for (Phone *phone in phones) {
            [self.managedObjectContext deleteObject:phone];
        }
        [self.managedObjectContext deleteObject:self.users[indexPath.row]];
        [self reload];
        
        [self.managedObjectContext save:nil];
    }
}

@end
