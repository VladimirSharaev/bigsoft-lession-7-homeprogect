//
//  User+Creation.m
//  CoreDataHomeProject
//
//  Created by Vladimir on 02.05.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import "User+Creation.h"
#import "AppDelegate.h"

@implementation User (Creation)

+ (User *)userWithNameAndAge:(NSString *)name age:(NSNumber *)age
{
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    NSManagedObjectContext *contex = appDelegate.managedObjectContext;
    
    User *user = (User * )[NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass(self) inManagedObjectContext:contex];
    if (user) {
        user.name = name;
        user.age = age;
    }
    return user;
}

@end
