//
//  PhoneTableViewController.h
//  CoreDataHomeProject
//
//  Created by Vladimir on 03.05.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"

@interface PhoneTableViewController : UITableViewController

@property (nonatomic) NSManagedObjectContext *managedObjectContext;
@property (nonatomic) User *user;


- (void)reload;
- (IBAction)addButtonWasPressed:(id)sender;

@end
