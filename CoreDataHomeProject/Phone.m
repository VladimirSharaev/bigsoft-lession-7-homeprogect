//
//  Phone.m
//  CoreDataHomeProject
//
//  Created by Vladimir on 02.05.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import "Phone.h"
#import "User.h"


@implementation Phone

@dynamic number;
@dynamic user;

@end
