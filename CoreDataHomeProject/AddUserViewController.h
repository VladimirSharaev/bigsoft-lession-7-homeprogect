//
//  AddUserViewController.h
//  CoreDataHomeProject
//
//  Created by Vladimir on 03.05.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AddUserViewControllerDelegate <NSObject>

- (void)addUser:(NSString *)name age:(NSNumber *)age;

@end

@interface AddUserViewController : UIViewController

#pragma mark - Propertes

@property (weak, nonatomic) id <AddUserViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UITextField *nameTextBox;
@property (weak, nonatomic) IBOutlet UITextField *ageTextBox;

#pragma mark - Actions

- (IBAction)addUserButtonWasPressed:(id)sender;
@end
